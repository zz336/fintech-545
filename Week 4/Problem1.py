import numpy as np


# Parameters
np.random.seed(42)
initial_price = 100  # Initial price at time t-1

periods = 20
# Simulate returns following a normal distribution
random_returns = np.random.normal(0, 0.1, size=periods)


def simulate_prices_classical(initial_price, returns):
    return initial_price + returns

def simulate_prices_arithmetic(initial_price, returns):
    return initial_price *(1+returns)


def simulate_prices_log(initial_price, returns):
    return initial_price * np.exp(returns)


# Calculate Pt+1 using classical, arithmetic, and logarithmic methods
classical_prices = simulate_prices_classical(initial_price, random_returns)
arithmetic_prices = simulate_prices_arithmetic(initial_price, random_returns)
log_prices = simulate_prices_log(initial_price, random_returns)

# Calculate mean and variance for each method
classical_mean = np.mean(classical_prices)
classical_variance = np.std(classical_prices)

arithmetic_mean = np.mean(arithmetic_prices)
arithmetic_variance = np.std(arithmetic_prices)

log_mean = np.mean(log_prices)
log_variance = np.std(log_prices)

# Print mean and variance for each method
print("Classical Method:")
print("Mean:", classical_mean)
print("standard deviation:", classical_variance)

print("\nArithmetic Method:")
print("Mean:", arithmetic_mean)
print("standard deviation:", arithmetic_variance)

print("\nLogarithmic Method:")
print("Mean:", log_mean)
print("standard deviation:", log_variance)