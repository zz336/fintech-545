import pandas as pd
import numpy as np
from scipy.stats import t
from scipy.optimize import minimize 
from statsmodels.tsa.arima.model import ARIMA


# Load the CSV file into a DataFrame, calculate returns
daily_prices = pd.read_csv("DailyPrices.csv")

# Calculate arithmetic returns for all prices
arithmetic_returns = daily_prices.copy()  # Create a copy of the DataFrame
for col in arithmetic_returns.columns[1:]:  # Exclude the date column
    arithmetic_returns[col] = (arithmetic_returns[col] / arithmetic_returns[col].shift(1)) - 1

# Drop the first row which contains NaN values
arithmetic_returns.dropna(inplace=True)


#Select the META column
meta_returns = arithmetic_returns['META']

mean_meta = meta_returns.mean()

centered_meta_returns = meta_returns - mean_meta

sd = centered_meta_returns.std()



# Calculate  Normal distribution
def cal_VaR_norm(returns, n=10000, alpha=0.05):
    mu = returns.mean()
    sigma = returns.std()
    simu_returns = np.random.normal(mu, sigma, n)
    simu_returns.sort()
    VaR_normal = -np.percentile(simu_returns, alpha*100)
    return VaR_normal

VaR_normal = cal_VaR_norm(centered_meta_returns)

print("Normal - VaR at 5% significance level:", VaR_normal)



# Normal distirbution + EWMA
def exp_weighted_cov(input, lambda_=0.97):
    ror = input.values
    ror_mean = np.mean(ror, axis=0)
    dev = ror - ror_mean
    times = dev.shape[0]
    weights = np.zeros(times)
    
    for i in range(times):
        weights[times - i - 1]  = (1 - lambda_) * lambda_**i
    
    weights_mat = np.diag(weights/sum(weights))

    cov = np.transpose(dev) @ weights_mat @ dev
    return cov

exp_simu_returns = np.random.normal(centered_meta_returns.mean(), np.sqrt(exp_weighted_cov(centered_meta_returns, lambda_=0.94)), 10000)
VaR_ew = -np.percentile(exp_simu_returns, 0.05*100)
print("EW normal - VaR at 5% significance level:", VaR_ew)


# MLE t distribution
params = t.fit(centered_meta_returns.values)

df = params[0]
scale = params[2]
VaR_t = -t.ppf(0.05, df=df, scale=scale)
print("MLE T- VaR at 5% significance level:", VaR_t)



# AR(1)

def cal_VaR_AR1(returns, n=10000, alpha=0.05):
    #a more general model that extends the ARMA model to non-stationary time series data.
    model = ARIMA(returns, order=(1, 0, 0)).fit()
    sigma = np.std(model.resid)
    sim_returns = np.empty(n)
    returns = returns.values
    for i in range(n):
        sim_returns[i] =  model.params[0] * (returns[-1]) + sigma * np.random.normal()
    VaR_AR1 = -np.percentile(sim_returns, alpha*100)
    return VaR_AR1


print("Ar1 VaR at 5% significance level:", cal_VaR_AR1(centered_meta_returns))


# Histroci simulation

VaR_hist = -np.percentile(centered_meta_returns, 0.05*100)

print("VaR histotical at 5% significance level:", VaR_hist)