import pandas as pd
import numpy as np
from scipy import stats
from scipy.optimize import minimize 
from statsmodels.tsa.arima.model import ARIMA

portfolio = pd.read_csv("portfolio.csv")

prices = pd.read_csv('DailyPrices.csv')

def return_calculate(prices, method="DISCRETE", dateColumn="Date"):
    vars = prices.columns
    nVars = len(vars)
    vars = [var for var in vars if var != dateColumn]
    if nVars == len(vars):
        raise ValueError("dateColumn: " + dateColumn + " not in DataFrame: " + str(vars))
    nVars = nVars - 1

    p = np.matrix(prices[vars])
    n = p.shape[0]
    m = p.shape[1]
    p2 = np.empty((n-1,m))

    for i in range(n-1):
        for j in range(m):
            p2[i,j] = p[i+1,j] / p[i,j]

    if method.upper() == "DISCRETE":
        p2 = p2 - 1.0
    elif method.upper() == "LOG":
        p2 = np.log(p2)
    else:
        raise ValueError("method: " + method + " must be in (\"LOG\",\"DISCRETE\")")

    dates = prices[dateColumn][1:n]
    out = pd.DataFrame({dateColumn: dates})
    for i in range(nVars):
        out[vars[i]] = p2[:,i]
    return out


def exp_weighted_cov(input, lambda_=0.97):
    ror = input.values
    ror_mean = np.mean(ror, axis=0)
    dev = ror - ror_mean
    times = dev.shape[0]
    weights = np.zeros(times)
    
    for i in range(times):
        weights[times - i - 1]  = (1 - lambda_) * lambda_**i
    
    weights_mat = np.diag(weights/sum(weights))

    cov = np.transpose(dev) @ weights_mat @ dev
    return cov

def get_single_portfolio(portfolio, prices, code):
    assets = portfolio[portfolio["Portfolio"] == code]
        
    stocks = list(assets["Stock"])
    assets_prices = prices[["Date"] + stocks].copy()
    
    Total_Value = prices[stocks].tail(1).values.dot(assets["Holding"].values)[0]
    holdings = assets["Holding"].values

    asset_values = holdings.reshape(-1, 1) * prices[stocks].tail(1).T.values
    delta = asset_values / Total_Value
    
    return Total_Value, assets_prices, holdings, delta

def get_all_portfolio(portfolio, prices):
    assets = portfolio.drop('Portfolio',axis=1)
    assets = assets.groupby(["Stock"], as_index=False)["Holding"].sum()
        
    stocks = list(assets["Stock"])
    assets_prices = prices[["Date"] + stocks].copy()
    
    Total_Value = prices[stocks].tail(1).values.dot(assets["Holding"].values)[0]
    holdings = assets["Holding"].values

    asset_values = holdings.reshape(-1, 1) * prices[stocks].tail(1).T.values
    delta = asset_values / Total_Value
    
    return Total_Value, assets_prices, holdings, delta


Total_Value_A, assets_prices_A, holdings_A, delta_A = get_single_portfolio(portfolio, prices, "A")
Total_Value_B, assets_prices_B, holdings_B, delta_B = get_single_portfolio(portfolio, prices, "B")
Total_Value_C, assets_prices_C, holdings_C, delta_C = get_single_portfolio(portfolio, prices, "C")
Total_Value_All, assets_prices_All, holdings_All, delta_All = get_all_portfolio(portfolio, prices)



def cal_delta_VaR(Total_Value, assets_prices, delta, alpha=0.05, lambda_=0.94):
    returns = return_calculate(assets_prices).drop('Date', axis=1)
    assets_cov = exp_weighted_cov(returns, lambda_)
    
    delta_norm_VaR = -Total_Value * stats.norm.ppf(alpha) * np.sqrt(delta.T @ assets_cov @ delta)
    
    return delta_norm_VaR.item()

delta_var_A = cal_delta_VaR(Total_Value_A, assets_prices_A, delta_A)
delta_var_B = cal_delta_VaR(Total_Value_B, assets_prices_B, delta_B)
delta_var_C = cal_delta_VaR(Total_Value_C, assets_prices_C, delta_C)
delta_var_All = cal_delta_VaR(Total_Value_All, assets_prices_All, delta_All)
print("Delta Normal VaR of portfolio A is ${}".format(delta_var_A))
print("Delta Normal VaR of portfolio B is ${}".format(delta_var_B))
print("Delta Normal VaR of portfolio C is ${}".format(delta_var_C))
print("Delta Normal VaR of All is ${}".format(delta_var_All))

##### change to historical method VaR

def cal_hist_VaR(assets_prices, holdings, alpha=0.05):
    returns = return_calculate(assets_prices).drop("Date", axis=1)
    assets_prices = assets_prices.drop('Date',axis=1)
    simu_returns = returns.sample(1000, replace=True)
    simu_prices = np.dot(simu_returns* assets_prices.tail(1).values.reshape(assets_prices.shape[1],),holdings)

    hist_VaR = -np.percentile(simu_prices, alpha*100)

    return hist_VaR

hist_var_A = cal_hist_VaR(assets_prices_A, holdings_A)
hist_var_B = cal_hist_VaR(assets_prices_B, holdings_B)
hist_var_C = cal_hist_VaR(assets_prices_C, holdings_C)
hist_var_All = cal_hist_VaR(assets_prices_All, holdings_All)
print("Historical VaR of portfolio A is ${}".format(hist_var_A))
print("Historical VaR of portfolio B is ${}".format(hist_var_B))
print("Historical VaR of portfolio C is ${}".format(hist_var_C))
print("Historical VaR of All is ${}".format(hist_var_All))


