######## Problem 1 

import numpy as np
import scipy.stats
from scipy.stats import t, describe
import csv

with open("problem1.csv",'r') as file:
    reader = csv.DictReader(file)
    data = [row for row in reader]
    nlist = []
    for x in data:
        nlist.append(float(x['x']))


### (a), calculate using formula


def first4Moments(sample):
    n = len(sample)

    # mean
    μ_hat = np.sum(sample) / n

    # remove the mean from the sample
    sim_corrected = sample - μ_hat
    cm2 = np.dot(sim_corrected, sim_corrected) / n

    # variance
    σ2_hat = np.dot(sim_corrected, sim_corrected) / (n - 1)

    # skewness
    skew_hat = np.sum(sim_corrected**3) / (n * np.sqrt(cm2**3))

    # kurtosis
    kurt_hat = np.sum(sim_corrected**4) / (n * cm2**2)

    excessKurt_hat = kurt_hat - 3

    return μ_hat, σ2_hat, skew_hat, excessKurt_hat



### (b) Calculate using package

moments = scipy.stats.describe(nlist)
moments_from_pack = [moments.mean, moments.variance, moments.skewness, moments.kurtosis]
moments_formula = first4Moments(nlist)
print(first4Moments(nlist))
print(moments_from_pack)



### (c) Test if is biased


# Mean
t_stat = (moments_from_pack[0] - moments_formula[0])/ np.sqrt(moments.variance / moments.nobs)
p_value_for_mean = 2 * (1 - t.cdf(np.abs(t_stat), df=moments.nobs - 1))

if np.any(p_value_for_mean < 0.05):
    print("Mean is significantly different from mu.")
else:
    print("Mean is not significantly different from mu.")

# Variance
t_stat = (moments_from_pack[1] - moments_formula[1])/ np.sqrt(moments.variance / moments.nobs)
p_value_for_var = 2 * (1 - t.cdf(np.abs(t_stat), df=moments.nobs - 1))

if np.any(p_value_for_var < 0.05):
    print("Variance is significantly different from sigma.")
else:
    print("Variance is not significantly different from sigma.")

# Skewness
t_stat = (moments_from_pack[2] - moments_formula[2])/ np.sqrt(moments.variance / moments.nobs)
p_value_for_skew = 2 * (1 - t.cdf(np.abs(t_stat), df=moments.nobs - 1))

if np.any(p_value_for_skew < 0.05):
    print("Skewness is significantly different from pop_skew.")
else:
    print("Skewness is not significantly different from pop_skew.")

# Kurtosis
t_stat = (moments_from_pack[3] - moments_formula[3])/ np.sqrt(moments.variance / moments.nobs)
p_value_for_kurt = 2 * (1 - t.cdf(np.abs(t_stat), df=moments.nobs - 1))

if np.any(p_value_for_kurt < 0.05):
    print("Mean is significantly different from pop_kurt.")
else:
    print("Mean is not significantly different from pop_kurt.")
