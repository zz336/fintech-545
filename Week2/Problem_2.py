import pandas as pd
import statsmodels.api as sm
import numpy as np
from scipy.optimize import minimize 
from scipy.stats import norm, t, multivariate_normal
from scipy import stats
import matplotlib.pyplot as plt

data= pd.read_csv("problem2.csv")

independent_variable = ''

ols_x = sm.add_constant(data['x'])
ols_y = data['y']

### OLS

model = sm.OLS(ols_y, ols_x)
results = model.fit()

beta0 = results.params['const']
beta1 = results.params['x']
std_error = np.std(results.resid)

print("OLS Beta (intercept):", beta0)
print("OLS Beta (slope):", beta1)
print("Standard Deviation of OLS error:", std_error)


#### MLE
x = data[['x']].values
y = data['y'].values

n = len(y)


x = np.column_stack([np.ones(n), x.flatten()])


def neg_log_likelihood_normal(params, x, y):
    beta0, beta1, sigma = params
    beta = np.array([beta0, beta1])

    x = np.column_stack([np.ones_like(y), x])  

    e = y - x @ beta
    s2 = sigma ** 2
    ll = norm.logpdf(e / sigma).sum() - len(y) / 2 * np.log(s2 * np.pi) - np.dot(e.T, e) / (2 * s2)
    return -ll

def neg_log_likelihood_t(params, x, y):
    beta0, beta1, sigma, nu = params
    pred = beta0 + beta1 * x
    neg_LL = -1 * np.sum(stats.t.logpdf(y - pred, sigma, scale=nu))

    return neg_LL

initial_params_normal = [1.0, 1.0, 1.0]
initial_params_t = [1.0, 1.0, 1.0, 1.0]

result_normal = minimize(neg_log_likelihood_normal, initial_params_normal, args=(x[:, 1], y), method='Nelder-Mead')
result_t = minimize(neg_log_likelihood_t, initial_params_t, args=(x[:, 1], y), method='Nelder-Mead')

if result_normal.success:
    beta0_optimal_normal, beta1_optimal_normal, sigma_optimal_normal = result_normal.x

    print("\nNormal Distribution Results:")
    print("Intercept (Beta0):", beta0_optimal_normal)
    print("Slope (Beta1):", beta1_optimal_normal)
    print("Standard Deviation (sigma):", sigma_optimal_normal)

if result_t.success:
    beta0_optimal_t, beta1_optimal_t, sigma_optimal_t, nu_optimal_t = result_t.x
    print("\nT Distribution Results:")
    print("Intercept (Beta0):", beta0_optimal_t)
    print("Slope (Beta1):", beta1_optimal_t)
    print("Standard Deviation (sigma):", nu_optimal_t) #### 改变
    print("Degrees of Freedom (nu):", sigma_optimal_t)


### Test which is best of fit USING AIC/BIC
AIC_norm = 2 * 3 - 2 * (-result_normal.fun)
AIC_t = 2 * 4 - 2 * (-result_t.fun)
print("AIC_norm < AIC_t:", AIC_norm < AIC_t)


#problem 2.c
data_2c = pd.read_csv('problem2_x.csv')
data_x1 = pd.read_csv('problem2_x1.csv')
mean = data_2c.mean()
cov = data_2c.cov()
x_1 = data_x1['x1']

#create model

multi_norm = multivariate_normal(mean=mean, cov=cov)
def condi_dis(x1, mean, cov):
    mean_1, mean_2 = mean
    cov_xx, cov_xy, cov_yx, cov_yy = cov.values.flatten()
    condi_mean = mean_2 + cov_yx / cov_xx * (x1 - mean_1)
    condi_cov = cov_yy - cov_yx * cov_xy / cov_xx
    return condi_mean, condi_cov

#store bounds
condi_meanlist = []
condi_covlist = []
lower_interval = []
upper_interval = []



mean_vector = data_2c.mean().values
covariance_matrix = data_2c.cov().values

print("Estimated mean vector:", mean_vector)
print("Estimated covariance matrix:\n", covariance_matrix)


#get expected value bounds
for x1 in x_1:
    condi_mean, condi_cov = condi_dis(x1, mean, cov)
    condi_meanlist.append(condi_mean)
    interval = 1.96 * np.sqrt(condi_cov)
    lower_interval.append(condi_mean - interval)
    upper_interval.append(condi_mean + interval)

#plot the graph 
plt.figure(figsize=(10, 6))
plt.plot(x_1, condi_meanlist, color='red', label='Expected x2')
plt.fill_between(x_1, lower_interval, upper_interval, color='gray', alpha=0.5, label="95% CI")
plt.xlabel('x1')
plt.ylabel('Explected x2')
plt.legend()
plt.show()