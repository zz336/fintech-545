
import pandas as pd
from statsmodels.tsa.arima.model import ARIMA


data= pd.read_csv("problem3.csv")

xvalue = data['x'].values

order_ar1 = (1, 0, 0)  # AR(1)
order_ar3 = (3, 0, 0)  # AR(3)
order_ma1 = (0, 0, 1)  # MA(1)
order_ma3 = (0, 0, 3)  # MA(3)

arima_model_ar1 = ARIMA(xvalue, order=order_ar1)
results_ar1 = arima_model_ar1.fit()
aic_ar1 = results_ar1.aic
bic_ar1 = results_ar1.bic

arima_model_ar3 = ARIMA(xvalue, order=order_ar3)
results_ar3 = arima_model_ar3.fit()
aic_ar3 = results_ar3.aic
bic_ar3 = results_ar3.bic

arima_model_ma1 = ARIMA(xvalue, order=order_ma1)
results_ma1 = arima_model_ma1.fit()
aic_ma1 = results_ma1.aic
bic_ma1 = results_ma1.bic

arima_model_ma3 = ARIMA(xvalue, order=order_ma3)
results_ma3 = arima_model_ma3.fit()
aic_ma3 = results_ma3.aic
bic_ma3 = results_ma3.bic

best_model_AIC = min([(aic_ar1, 'AR(1)'), (aic_ar3, 'AR(3)'), (aic_ma1, 'MA(1)'), (aic_ma3, 'MA(3)')])[1]
print("\nBest Fit Model based on AIC:", best_model_AIC)
best_model_BIC = min([(bic_ar1, 'AR(1)'), (bic_ar3, 'AR(3)'), (bic_ma1, 'MA(1)'), (bic_ma3, 'MA(3)')])[1]
print("\nBest Fit Model based on BIC:", best_model_BIC)

