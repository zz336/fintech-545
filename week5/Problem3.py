import pandas as pd
import numpy as np
from scipy import stats
from scipy.optimize import minimize 
from statsmodels.tsa.arima.model import ARIMA


portfolio = pd.read_csv("portfolio.csv")

prices = pd.read_csv('DailyPrices.csv')


def get_single_portfolio(portfolio, prices, code):
    assets = portfolio[portfolio["Portfolio"] == code]
        
    stocks = list(assets["Stock"])
    assets_prices = prices[["Date"] + stocks].copy()
    
    Total_Value = prices[stocks].tail(1).values.dot(assets["Holding"].values)[0]
    holdings = assets["Holding"].values

    asset_values = holdings.reshape(-1, 1) * prices[stocks].tail(1).T.values
    #delta = asset_values / Total_Value
    return Total_Value, assets_prices, holdings


def get_all_portfolio(portfolio, prices):
    assets = portfolio.drop('Portfolio',axis=1)
    assets = assets.groupby(["Stock"], as_index=False)["Holding"].sum()
        
    stocks = list(assets["Stock"])
    assets_prices = prices[["Date"] + stocks].copy()
    
    Total_Value = prices[stocks].tail(1).values.dot(assets["Holding"].values)[0]
    holdings = assets["Holding"].values

    asset_values = holdings.reshape(-1, 1) * prices[stocks].tail(1).T.values
    
    return Total_Value, assets_prices, holdings


def return_calculate(prices, method="DISCRETE", dateColumn="Date"):
    vars = prices.columns
    nVars = len(vars)
    vars = [var for var in vars if var != dateColumn]
    if nVars == len(vars):
        raise ValueError("dateColumn: " + dateColumn + " not in DataFrame: " + str(vars))
    nVars = nVars - 1

    p = np.matrix(prices[vars])
    n = p.shape[0]
    m = p.shape[1]
    p2 = np.empty((n-1,m))

    for i in range(n-1):
        for j in range(m):
            p2[i,j] = p[i+1,j] / p[i,j]

    if method.upper() == "DISCRETE":
        p2 = p2 - 1.0
    elif method.upper() == "LOG":
        p2 = np.log(p2)
    else:
        raise ValueError("method: " + method + " must be in (\"LOG\",\"DISCRETE\")")

    dates = prices[dateColumn][1:n]
    out = pd.DataFrame({dateColumn: dates})
    for i in range(nVars):
        out[vars[i]] = p2[:,i]
    return out


Total_Value_A, assets_prices_A, holdings_A = get_single_portfolio(portfolio, prices, "A")
Total_Value_B, assets_prices_B, holdings_B = get_single_portfolio(portfolio, prices, "B")
Total_Value_C, assets_prices_C, holdings_C = get_single_portfolio(portfolio, prices, "C")
Total_Value_All, assets_prices_All, holdings_All, delta_All = get_all_portfolio(portfolio, prices)


print(return_calculate(assets_prices_A))
print(return_calculate(assets_prices_B))
print(return_calculate(assets_prices_C))
print(return_calculate(Total_Value_All))



def MLE_t(params, returns):
    df, loc, scale = params
    neg_LL = -1 * np.sum(stats.t.logpdf(returns, df=df, loc=loc, scale=scale))
    return(neg_LL)

def fit_MLE_t(returns, n=10000, alpha=0.05):
    constraints = [
        {'type': 'ineq', 'fun': lambda x: x[0] - 1},
        {'type': 'ineq', 'fun': lambda x: x[2]}
    ]
    
    res = minimize(MLE_t, x0=[10, returns.mean(), returns.std()], args=(returns,), constraints=constraints)
    
    df, loc, scale = res.x[0], res.x[1], res.x[2]
    return df, loc, scale