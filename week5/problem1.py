import numpy as np
import pandas as pd
from scipy.linalg import cholesky, eigvals
from scipy import stats
from scipy.optimize import minimize 
from statsmodels.tsa.arima.model import ARIMA
import time

from scipy import optimize

import quant_risk_mgmt as qrm



df = pd.read_csv("DailyPrices.csv")
problem1 = pd.read_csv('problem1.csv')


# Calculate arthmetic return
df_return = qrm.return_calculate(df)
df_return.drop('Date', axis=1, inplace=True)






# Cov estimation 2.1
ew_cov = qrm.exp_weighted_cov(df_return)
pd.DataFrame(ew_cov)

# 2.2
ew_corr = qrm.exp_weighted_corr(df_return)
pd.DataFrame(ew_corr)









# Non - PSD test 

n = 500
sigma = np.full((n,n),0.9)
for i in range(n):
    sigma[i,i]=1.0
sigma[0,1] = 0.7357
sigma[1,0] = 0.7357

# 3.1 3.2
print("Sigma fixed with near_psd(): ", qrm.is_psd(qrm.near_psd(pd.read_csv("testout_1.3.csv"))))
print("Sigma fixed with near_psd(): ", qrm.is_psd(qrm.near_psd(pd.read_csv("testout_1.4.csv"))))

# 3.3 3.4
print("Sigma fixed with Higham_psd(): ", qrm.is_psd(qrm.Higham_psd(pd.read_csv("testout_1.3.csv"))))
print("Sigma fixed with Higham_psd(): ", qrm.is_psd(qrm.Higham_psd(pd.read_csv("testout_1.4.csv"))))


# 4.1
qrm.chol_psd(pd.read_csv("testout_3.1.csv"))


# 5.1 
qrm.simulateNormal(100000, df, mean=None, seed=1234)

# 5.2
qrm.simulateNormal(100000, df, mean=None, seed=1234)

# 5.3
qrm.simulateNormal(100000, df, mean=None, seed=1234, fixMethod=qrm.near_psd)

# 5.4
qrm.simulateNormal(100000, df, mean=None, seed=1234, fixMethod=qrm.Higham_psd)

# 5.5
qrm.simulatePCA(100000, df, mean=None, seed=1234, pctExp=1)


#6.1
qrm.return_calculate(pd.read_csv("test6.csv"))
#6.2
qrm.return_calculate(pd.read_csv("test6.csv"),method="LOG")




#7.1
qrm.fit_normal(pd.read_csv("test7_1.csv"))


#7.2
qrm.fit_t(pd.read_csv("test7_2.csv"))

#7.3
qrm.fit_regression_t(pd.read_csv("test7_3.csv"))



#8.1
qrm.cal_VaR_norm(pd.read_csv("test7_1.csv"))


## 8.2
qrm.calculate_VaR_t(pd.read_csv("test7_2.csv"))



## 8.5
qrm.cal_VaR_MLE_t(pd.read_csv("test7_2.csv"))

## 8.6
es_hist = qrm.ES(pd.read_csv("test7_2.csv"))






# 8.5


# Simulation method

pd.DataFrame(qrm.multivar_norm_simu(ew_cov))
pd.DataFrame(qrm.multivar_norm_simu(ew_cov, method='pca'))

#1. Pearson correlation and var
p_cov_var = qrm.p_cov_var_f(problem1)
#2. Pearson correlation and EW variance
p_corr_ew_var = qrm.p_corr_ew_var_f(problem1)
#3. EW(cov+corr) is exp_weighted_cov(input, lambda_)
ew_corr_cov = qrm.exp_weighted_cov(problem1)
#4. EW Corr +Var
ew_corr_p_var = qrm.ew_corr_p_var_f(problem1)

def time_and_error(covariance_matrix, method, explained_variance=1.0):
    
    start_time = time.time()
    samples = qrm.multivar_norm_simu(covariance_matrix, method=method, explained_variance=explained_variance)
    time_used = time.time() - start_time
    
    simulated_covariance = np.cov(samples, rowvar=False)
    error = qrm.Frobenius(simulated_covariance - covariance_matrix)
    
    return time_used, error
def simulate4(covariance_matrix):
    np.random.seed(0)
    variances = [1.0, 0.75, 0.5]
    
    time_used = []
    errors = []
    labels = []
    
    t, error = time_and_error(covariance_matrix, 'direct')
    time_used.append(t)
    errors.append(error)
    labels.append("direct")
    
    for var in variances:
        t, error = time_and_error(covariance_matrix, 'pca', var)
        time_used.append(t)
        errors.append(error)
        labels.append("PCA with {}".format(var))
            

## VaR calculation
        

qrm.cal_VaR_ES_norm(problem1)
qrm.cal_VaR_MLE_t(problem1)
qrm.cal_VaR_AR1(problem1)
qrm.cal_VaR_hist(problem1.values)



